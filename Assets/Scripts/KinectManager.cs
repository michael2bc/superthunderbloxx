﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class KinectManager : MonoBehaviour {

  public GameObject armPrefab;
  [HideInInspector]
  public GameObject bodyObjectParent;
  
  [HideInInspector]
  public Vector3 originPt;
  private Body currentBody;
  private BodySourceManager bodyManager;
  private Dictionary<JointType, JointType> _BoneMap = new Dictionary<JointType, JointType>(){
      { JointType.FootLeft, JointType.AnkleLeft },
      { JointType.AnkleLeft, JointType.KneeLeft },
      { JointType.KneeLeft, JointType.HipLeft },
      { JointType.HipLeft, JointType.SpineBase },
      
      { JointType.FootRight, JointType.AnkleRight },
      { JointType.AnkleRight, JointType.KneeRight },
      { JointType.KneeRight, JointType.HipRight },
      { JointType.HipRight, JointType.SpineBase },
      
      { JointType.HandTipLeft, JointType.HandLeft },
      { JointType.ThumbLeft, JointType.HandLeft },
      { JointType.HandLeft, JointType.WristLeft },
      { JointType.WristLeft, JointType.ElbowLeft },
      { JointType.ElbowLeft, JointType.ShoulderLeft },
      { JointType.ShoulderLeft, JointType.SpineShoulder },
      
      { JointType.HandTipRight, JointType.HandRight },
      { JointType.ThumbRight, JointType.HandRight },
      { JointType.HandRight, JointType.WristRight },
      { JointType.WristRight, JointType.ElbowRight },
      { JointType.ElbowRight, JointType.ShoulderRight },
      { JointType.ShoulderRight, JointType.SpineShoulder },
      
      { JointType.SpineBase, JointType.SpineMid },
      { JointType.SpineMid, JointType.SpineShoulder },
      { JointType.SpineShoulder, JointType.Neck },
      { JointType.Neck, JointType.Head },
  };


  void Awake(){
    bodyManager = GetComponent<BodySourceManager>();
  }

	void Update () {
    if(bodyManager != null && bodyManager.GetData() != null){
      if(currentBody != null){
        if(!currentBody.IsTracked){
          currentBody = null;
          return;
        }
        RefreshBodyObject(currentBody,bodyObjectParent);
        bodyObjectParent.transform.localPosition = Vector3.zero;
        //FitColliderToChildren(bodyObjectParent);
      }else{
        if(bodyObjectParent != null) Destroy(bodyObjectParent);
        currentBody = GetNewBody();
        if(currentBody != null) bodyObjectParent = GenerateNewBody(currentBody);
      }
    }
		
	}

  public bool Tracking(){
    return currentBody != null && bodyObjectParent != null;
  }

  public Vector3 GetOrigin(Body bodyToGet = null){
    if(bodyToGet == null && currentBody != null) bodyToGet = currentBody;
    else return Vector3.zero;
    return GetVector3FromJoint(bodyToGet.Joints[JointType.SpineMid]) + (GetVector3FromJoint(bodyToGet.Joints[JointType.SpineBase]) - GetVector3FromJoint(bodyToGet.Joints[JointType.SpineMid]))/2;
  }

  public float GetArmSpan(){
    return
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.HandLeft]),GetVector3FromJoint(currentBody.Joints[JointType.WristLeft])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.WristLeft]),GetVector3FromJoint(currentBody.Joints[JointType.ElbowLeft])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.ElbowLeft]),GetVector3FromJoint(currentBody.Joints[JointType.ShoulderLeft])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.SpineShoulder]),GetVector3FromJoint(currentBody.Joints[JointType.ShoulderRight])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.ShoulderRight]),GetVector3FromJoint(currentBody.Joints[JointType.ElbowRight])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.ElbowRight]),GetVector3FromJoint(currentBody.Joints[JointType.WristRight])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.WristRight]),GetVector3FromJoint(currentBody.Joints[JointType.HandRight]))
      ;
  }

  public float GetUserHeight(){
    return
      ((Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.AnkleLeft]),GetVector3FromJoint(currentBody.Joints[JointType.KneeLeft])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.KneeLeft]),GetVector3FromJoint(currentBody.Joints[JointType.HipLeft])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.AnkleRight]),GetVector3FromJoint(currentBody.Joints[JointType.KneeRight])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.KneeRight]),GetVector3FromJoint(currentBody.Joints[JointType.HipRight])))/2) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.SpineBase]),GetVector3FromJoint(currentBody.Joints[JointType.SpineMid])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.SpineMid]),GetVector3FromJoint(currentBody.Joints[JointType.SpineShoulder])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.SpineShoulder]),GetVector3FromJoint(currentBody.Joints[JointType.Neck])) +
      Vector3.Distance(GetVector3FromJoint(currentBody.Joints[JointType.Neck]),GetVector3FromJoint(currentBody.Joints[JointType.Head]))
      ;
  }

  Body GetNewBody(){
    Body[] tmpBodies = bodyManager.GetData();
    if(tmpBodies == null) return null;
    List<Body> acceptableBodies = new List<Body>();
    for(int i = 0;i<tmpBodies.Length;i++){
      if(tmpBodies[i].IsTracked){
        acceptableBodies.Add(tmpBodies[i]);
      }
    }
    acceptableBodies.Sort(
      delegate(Body p1, Body p2){
        if(Vector3.Distance(GetOrigin(p1), originPt) < Vector3.Distance(GetOrigin(p2), originPt)){
          return -1;
        }else{
          return 1;
          }
      }
    );
    
    if(acceptableBodies.Count > 0){
      return acceptableBodies[0];
    }else{
      return null;
    }
  }
  GameObject GenerateNewBody(Body body){
    GameObject bodyObj = new GameObject("Body");
    
    for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++){
      GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
      jointObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
      jointObj.name = jt.ToString();
      jointObj.transform.parent = bodyObj.transform;

      Windows.Kinect.Joint sourceJoint = body.Joints[jt];
      Windows.Kinect.Joint? targetJoint = null;

      if(_BoneMap.ContainsKey(jt)){
        targetJoint = body.Joints[_BoneMap[jt]];
        GameObject armObj = Instantiate(armPrefab);//GameObject.CreatePrimitive(PrimitiveType.Capsule);
        armObj.transform.parent = jointObj.transform;
        armObj.name = "Arm";
      }
    }
    return bodyObj;
  }
  private void RefreshBodyObject(Body body, GameObject bodyObject){
    for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++){
      Windows.Kinect.Joint sourceJoint = body.Joints[jt];
      Windows.Kinect.Joint? targetJoint = null;
      
      if(_BoneMap.ContainsKey(jt)){
        targetJoint = body.Joints[_BoneMap[jt]];
      }
        
      Transform jointObj = bodyObject.transform.FindChild(jt.ToString());
      jointObj.localPosition = GetVector3FromJoint(sourceJoint);

      Transform armObj = jointObj.FindChild("Arm");
      if(armObj != null){
        armObj.position = jointObj.localPosition + (GetVector3FromJoint(targetJoint.Value) - jointObj.localPosition) /2;
        armObj.LookAt(jointObj.localPosition);
        Vector3 newSize = armObj.transform.localScale;
        newSize.z = (GetVector3FromJoint(targetJoint.Value) - jointObj.localPosition).magnitude;
        armObj.transform.localScale = newSize;
      }
    }
  }
  private static Vector3 GetVector3FromJoint(Windows.Kinect.Joint joint){
    return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
  }
}
