﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InputManagerUI : MonoBehaviour {

	public GameObject combo;

	public GameObject miss;
	public GameObject fail;
	public GameObject weak;

	public GameObject nice;
	public GameObject radical;
	public GameObject wicked;

	public GameObject posPFX1;
	public GameObject posPFX2;


	public GameObject negPFX1;
	public GameObject negPFX2;

	public GameObject gameOverTitle;

	public GameObject cameraAnim;
	public GameObject title;

	public GameObject centerPost;
	public GameObject gameUI;

	public GameObject insertCoin;
	public GameObject tPoseStart;
	public GameObject gameOver;

	public GameObject tPoseScreen;
	public GameObject tPosePFX;

	private GameObject currGameMusic;
	private GameObject currCenterPost;

	AudioSource audio;


	// Use this for initialization
	void Start () {

		audio = this.GetComponent<AudioSource> ();
		gameUI.SetActive (false);

		centerPost.SetActive (false);
		tPoseScreen.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.C)) {

			Instantiate(combo, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(posPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(posPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.M)) {

			Instantiate(miss, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(negPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(posPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.F)) {

			Instantiate(fail, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(negPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(negPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.W)) {

			Instantiate(weak, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(negPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(negPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.N)) {

			Instantiate(nice, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(posPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(posPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.R)) {

			Instantiate(radical, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(posPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(posPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown (KeyCode.X)) {

			Instantiate(wicked, new Vector3(0, 0, 0), Quaternion.identity);

			Instantiate(posPFX1, new Vector3(-1, -8.2f, 12.1f), Quaternion.identity);
			Instantiate(posPFX2, new Vector3(6.1f, -6, 12.1f), Quaternion.identity);

		}

		if (Input.GetKeyDown(KeyCode.S)) {

			//title.SetActive (false);

			DOTween.Play (cameraAnim);
			tPoseScreen.SetActive (false);
			tPoseStart.SetActive (true);

			gameUI.SetActive (true);
			centerPost.SetActive (true);
			Instantiate(tPosePFX, new Vector3(0, 0, 0), Quaternion.identity);
		}

		if (Input.GetKeyDown(KeyCode.KeypadEnter)){

			Instantiate(insertCoin, new Vector3(0, 0, 0), Quaternion.identity);


		}

		if (Input.GetKeyDown (KeyCode.G)) {

			gameUI.SetActive (false);
			centerPost.SetActive (false);

			Instantiate(gameOverTitle, new Vector3(0, 0, 0), Quaternion.identity);

			tPoseStart.GetComponent<AudioSource> ().Stop ();

		}

		if (Input.GetKeyDown (KeyCode.T)) {

			title.SetActive (false);
			tPoseScreen.SetActive (true);

		}
			
	}
}
