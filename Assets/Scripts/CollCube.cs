﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CollCube : MonoBehaviour {

  public Material[] mats; //onColor, neutralColor, deathColor;
  public AudioClip[] audioClips;
  //public bool isOn = false;

  public CubeType type = CubeType.Idle;
  public GameObject partEffect;

  private Renderer rend;
  private float partDuration = 1.5f;
  private AudioSource aud;

	void Awake () {
    rend = GetComponent<Renderer>();
    aud = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public void Configure(CubeType tmpType){
    type = tmpType;
    rend.material = mats[(int)(type)];//isOn ? onColor : neutralColor;
  }

  void OnTriggerEnter(Collider coll){
    if(coll.gameObject.tag == "Limb" && type != CubeType.Idle){
      aud.clip = audioClips[(int)(type)];
      aud.Play();

      switch(type){
        case CubeType.Point:
          GameManager.Instance.Scored();
          break;
        case CubeType.Death:
          GameManager.Instance.Missed();
          break;
      }
      GameObject tmpCube = Instantiate(partEffect);
      tmpCube.transform.parent = transform;
      tmpCube.transform.localPosition = Vector3.zero;
      tmpCube.transform.localEulerAngles = Vector3.zero;
      tmpCube.transform.localScale = Vector3.one;

      tmpCube.GetComponent<Renderer>().material = mats[(int)(type)];
      Color colWithoutA = mats[(int)(type)].color;
      colWithoutA.a = 0f;
      tmpCube.GetComponent<Renderer>().material.DOColor(colWithoutA, partDuration);
      tmpCube.transform.DOScale(tmpCube.transform.localScale*2f,partDuration);
      Destroy(tmpCube, partDuration);
      type = CubeType.Idle;
      rend.material = mats[(int)(type)];
    }
  }

  void OnTriggerExit(Collider coll){
    if(coll.gameObject.name == "Check"){
      if(type == CubeType.Point){ 
        GameManager.Instance.DidntHit();
      }
      type = CubeType.Idle;
      rend.material = mats[(int)(type)];
    }
  }
}
