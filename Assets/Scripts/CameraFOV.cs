﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFOV : MonoBehaviour {

	public GameObject gameCamera;

	public float gameFOV;

	public float speedFOV;

	public bool speedUp;

	// Use this for initialization
	void Start () {

		gameCamera.GetComponent<Camera> ().fieldOfView = gameFOV;
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Alpha0)) {

			speedUp = true;
		}

		if (speedUp == true) {

			gameCamera.GetComponent<Camera> ().fieldOfView = gameFOV + 1f;

		}

		if (gameFOV == speedFOV) {

			speedUp = false;
		}

		if (speedUp == false) {

			return;
		}

		
	}
}
