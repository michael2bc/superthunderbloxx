﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;
using DG.Tweening;

public class GameManager : MonoBehaviour {
  public GameObject startingUi, igUi;
  public Transform gameScene;
  public static GameManager Instance;
  public TextMesh topText, streakText;
  public Transform livesParent;
  public int minStreak = 3;
  public float bodyScaleFactor;

  private GameObject bodyObjectParent;

  [HideInInspector]
  public GameState currState = GameState.Idle, oldState;

  private RotScreen screen;

  private int numBlocks = 3, score = 0, lives, startingLives;
  private float rotSpeed = 50f, rotDelta = 0f;

  private int streak;

  private bool levelConfigured = false, gameOver = false, readyToStart = false;

  private KinectManager kinMgr;
  private Color colorToLerp = Color.white;
  private UnityEngine.AudioSource aud;

	void Awake () {
    if(Instance == null) Instance = this;
    else Destroy(gameObject);
    kinMgr = GetComponent<KinectManager>();
    screen = GameObject.FindGameObjectWithTag("Screen").GetComponent<RotScreen>();
    startingLives = livesParent.childCount;
    aud = GetComponent<UnityEngine.AudioSource>();
	}

	void Update () {
    startingUi.SetActive(currState == GameState.Idle);
    igUi.SetActive(currState == GameState.Playing || currState == GameState.End);
    if(Input.GetKeyDown(KeyCode.C)){
      gameScene.position = kinMgr.GetOrigin();
      kinMgr.originPt = gameScene.position;
    }
    switch(currState){
		case GameState.Idle:
			if (Input.GetKeyDown (KeyCode.Space))
				StartGame ();
			topText.text = "Hit the Green Block to START";
        if(readyToStart && kinMgr.Tracking()) currState = GameState.Config;
        break;
      case GameState.Config:
        gameOver = false;
        levelConfigured = false;
        readyToStart = false;
        score = 0;
        numBlocks = 3;
        rotDelta = 0f;
        screen.ConfigureRotScreen(kinMgr.GetArmSpan()/bodyScaleFactor, kinMgr.GetUserHeight()/bodyScaleFactor);
        lives = startingLives;
        aud.Play();
        currState = GameState.Playing;//GameState.FindBody;
        break;
      case GameState.Playing:
        if(gameOver){
          Invoke("MoveToIdle", 3f);
          topText.text = "You died. \n Final Score : " + score.ToString();
          Vector3 newAngles = screen.transform.eulerAngles;
          screen.EndGame();
          aud.Stop();
          currState = GameState.End;
          return;
        }
        if(kinMgr.Tracking()){
          if(!levelConfigured && InRange((int)(screen.transform.eulerAngles.y),0,75)){
            streak++;
            screen.NewLevel();
            rotDelta += 5f;
            numBlocks += (score % 10) == 0 ? 1 : 0;
            levelConfigured = true;
          }
          topText.text = score.ToString();
          screen.transform.Rotate(new Vector3(0,-1*(rotSpeed+rotDelta)*Time.deltaTime,0));
        }
        if(streak > minStreak && !DOTween.IsTweening(colorToLerp)){
          DOTween.To(()=> colorToLerp, x=> colorToLerp = x, Random.ColorHSV(), (minStreak*2)/streak).SetOptions(false).Play();
        }else if(streak < minStreak){
          colorToLerp = Color.white;
        }
        streakText.color = colorToLerp;
        streakText.text = "COMBO: " + streak.ToString() +"X";
        break;
      case GameState.End:
        break;
    }
    if(oldState != currState){
      Debug.Log(currState);
    }
    oldState = currState;
	}

  void MoveToIdle(){
    currState = GameState.Idle;
    for(int i = 0;i<livesParent.childCount;i++){
      GameObject liveIndicator = livesParent.GetChild(Mathf.Abs(i)).gameObject;
      liveIndicator.GetComponent<MeshRenderer>().material.DOColor(Color.white, 1f);
      liveIndicator.transform.DOScale(Vector3.one, 1f);
    }
  }

  bool InRange(int val, int min, int max){
    return val > min && val < max;
  }

  public void Missed(){
    lives -= 1;
    for(int i = startingLives;i>lives;i--){
      GameObject liveIndicator = livesParent.GetChild(Mathf.Abs(i-3)).gameObject;
      liveIndicator.GetComponent<MeshRenderer>().material.DOColor(Color.red, 1f);
      liveIndicator.transform.DOScale(new Vector3(1.5f,1.5f,1.5f), 1f);
    }
    if(lives <= 0){
      gameOver = true;
    }
    streak = 0;
  }
  public void AllOff(){
    levelConfigured = false;
  }
  public void DidntHit(){
    streak = 0;
  }
  public void Scored(){
    score += 1*streak;
  }
  public void StartGame(){
    readyToStart = true;
  }

}
