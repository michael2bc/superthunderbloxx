﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBlock : MonoBehaviour {

  void Awake(){
    GetComponent<Renderer>().material.color = Color.green;
  }
  void OnTriggerEnter(Collider coll){
    GameManager.Instance.StartGame();
  }
}
