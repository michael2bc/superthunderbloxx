﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingScript : MonoBehaviour {

	public float scrollSpeed = 0.9f;
	public float scrollSpeed2 = 0.9f;

	public Renderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		float offset = Time.time * scrollSpeed;
		float offset2 = Time.time * scrollSpeed2;

		rend.material.mainTextureOffset = new Vector2 (offset2, -offset);

	}
}
