﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotScreen : MonoBehaviour {

  public GameObject blockFab;
  public int numWidth, numHeight;
  [Range(0f,5f)]
  public float spacingX, spacingY;

  private CollCube[] allCubes;
  private int deathsPerPoints = 4;
  private Vector3 startingRotation;

  private GameObject screenObject;

  GameObject GenerateScreen(float width, float height, int numBlocksW, int numBlocksH){
    GameObject parent = new GameObject("ScreenParent");
    float blockScaleX = width/numBlocksW;
    float blockScaleY = height/numBlocksH;
    for(int i = 0;i<numBlocksH;i++){
      for(int j = 0;j<numBlocksW;j++){
        Vector3 blockPos = new Vector3((blockScaleX*j) + (spacingX*j), (blockScaleY*i) + (spacingY*i), 0f);
        GameObject tmpBlock = Instantiate(blockFab);
        tmpBlock.transform.parent = parent.transform;
        tmpBlock.transform.localPosition = blockPos;
        //tmpBlock.transform.localScale = new Vector3(blockScaleX, blockScaleY,tmpBlock.transform.localScale.z);
      }
    }
    return parent;
  }

  private void Awake(){
    startingRotation = transform.eulerAngles;
  }
  public void ConfigureRotScreen(float width, float height){
    if(screenObject != null) Destroy(screenObject);
    transform.eulerAngles = startingRotation;
    screenObject = GenerateScreen(width, height, numWidth, numHeight);
    screenObject.transform.parent = transform;
    screenObject.transform.localPosition = new Vector3(-2f, height/-2f, 0f);
    screenObject.transform.localScale = Vector3.one;
    allCubes = new CollCube[screenObject.transform.childCount];
    for(int i = 0; i<allCubes.Length;i++){
      GameObject tmpObj = screenObject.transform.GetChild(i).gameObject; 
      allCubes[i] = tmpObj.transform.GetChild(0).GetComponent<CollCube>();
      tmpObj.name = i.ToString();
    }
    Array.Reverse(allCubes);
  }

  public void EndGame(){
    Destroy(screenObject);
  }

  void Update(){
    if(GameManager.Instance.currState == GameState.Playing){
      bool check = false;
      foreach(CollCube x in allCubes){
        if(x.type != CubeType.Idle){
          check = true;
        }
      }
      if(!check){
        GameManager.Instance.AllOff();
      }
    }
  }

  private int[] GetUniqueVals(int length, int maxVal){
    int[] arrayToReturn = new int[length];
    int index = 0;
    for(int i =0;i<length;i++){
      int tmpVal = UnityEngine.Random.Range(0, maxVal);
      while((Array.Exists(arrayToReturn, element => element == tmpVal))){
        tmpVal = UnityEngine.Random.Range(0, maxVal);
      }
      arrayToReturn[index] = tmpVal;
      index++;
    }
    return arrayToReturn;
  }

  public void NewLevel(){
    //Debug.Log(Levels.level[UnityEngine.Random.Range(0,Levels.level.Length)]);

    int levelId = UnityEngine.Random.Range(0,Levels.level.GetUpperBound(0)-1);
    for(int i =0;i<numHeight;i++){
      string[] splitString = Levels.level[levelId,i].Split(',');
      for(int j =0;j<numWidth;j++){
        switch(splitString[j]){
          case ".":
            allCubes[(i*numWidth)+j].Configure(CubeType.Idle);
            break;
          case "x":
            allCubes[(i*numWidth)+j].Configure(CubeType.Point);
            break;
          case "/":
            allCubes[(i*numWidth)+j].Configure(CubeType.Death);
            break;
        }
      }
    }
  }
}
