---------------------------------------------
EasyShader
---------------------------------------------

A shader generator with togglable features.

Put the script on an object, select the features you want,
press "Generate" and an optimized shader is created for this object.

---------------------------------------------
Available features
---------------------------------------------

- specular
- specular map
- transparent
- cutout
- self illuminated
- normal map
- rim lighting
- reflection cubemap
- reflection intensity map
- outline
- two sided

---------------------------------------------
Details on how it works
---------------------------------------------

- The shader needs a name to be set in the EasyShader component
- The shader is generated in the EasyShader/Generated folder

---------------------------------------------
Contact
---------------------------------------------

info@aworldforus.com
