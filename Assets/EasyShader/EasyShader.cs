using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EasyShader : MonoBehaviour
{
  public string shaderName;

  public Material material;

  [HideInInspector] public bool twoSided;
  [HideInInspector] public bool transparent;
  [HideInInspector] public bool cutout;
  [HideInInspector] public bool specular;
  [HideInInspector] public bool specularMap;
  [HideInInspector] public bool selfIllum;
  [HideInInspector] public bool normalMap;
  [HideInInspector] public bool rim;
  [HideInInspector] public bool reflect;
  [HideInInspector] public bool reflectMap;
  [HideInInspector] public bool outline;
}