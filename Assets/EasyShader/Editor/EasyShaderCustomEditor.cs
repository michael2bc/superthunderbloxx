using UnityEditor;
using UnityEngine;
using System.IO;

[CustomEditor(typeof(EasyShader))]
public class EasyShaderCustomEditor: Editor {
 
  // InspectorGUI


  // Shader creation temporary variables
  private string properties_;
  private string imports_;
  private string renderType_;
  private string queue_;
  private string lightingModel_;
  private string fallback_;
  private string culling_;
  private string diffuse_;
  private string specular_;
  private string normal_;
  private string emission_;
  private string nextPasses_;
  private string inputs_;

  //------------------------------------------------------------------------------------------------
  // OnInspectorGUI
  //------------------------------------------------------------------------------------------------
  public override void OnInspectorGUI()
  {
      EasyShader easyShader = (EasyShader) target;
      
      // Defaults
      DrawDefaultInspector();

      // two sided
      easyShader.twoSided = GUILayout.Toggle(easyShader.twoSided, "Two sided");
      
      // Transparency options
      easyShader.transparent = GUILayout.Toggle(easyShader.transparent, "Transparent");

      if(easyShader.transparent)
        easyShader.cutout = GUILayout.Toggle(easyShader.cutout, "Cut out");

      // Specular Map
      easyShader.specular = GUILayout.Toggle(easyShader.specular, "Specular");
      if(easyShader.specular)
        easyShader.specularMap = GUILayout.Toggle(easyShader.specularMap, "Specular map");

      // Self Illum
      easyShader.selfIllum = GUILayout.Toggle(easyShader.selfIllum, "Self Illum");

      // Bump Map
      easyShader.normalMap = GUILayout.Toggle(easyShader.normalMap, "Normal map"); 

      // Rim
      easyShader.rim = GUILayout.Toggle(easyShader.rim, "Rim lighting"); 

      // Reflect
      easyShader.reflect = GUILayout.Toggle(easyShader.reflect, "Reflection Cubemap");  
      if(easyShader.reflect)
        easyShader.reflectMap = GUILayout.Toggle(easyShader.reflectMap, "Reflection map");   

      // Outline
      easyShader.outline = GUILayout.Toggle(easyShader.outline, "Outline");  

      // Generate button
      if(GUILayout.Button("Generate"))
      {
        string folderPath = Path.Combine(Application.dataPath, Path.Combine("EasyShader", "Generated"));
        string path = Path.Combine(Application.dataPath, Path.Combine("EasyShader", Path.Combine("Generated", easyShader.shaderName + ".shader")));

        if(!System.IO.Directory.Exists(folderPath))
          System.IO.Directory.CreateDirectory(folderPath);
  
        string shader = BuildShader(easyShader);
        if(shader != null)
        {
          File.Delete(path);
          File.AppendAllText(path, shader);
          AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);
          easyShader.material.shader = Shader.Find("EasyShader/Generated/"+easyShader.shaderName);
          if(easyShader.GetComponent<Renderer>().sharedMaterial == null)
            easyShader.GetComponent<Renderer>().sharedMaterial = easyShader.material;
        }
      }
  }

  //------------------------------------------------------------------------------------------------
  // BuildShader
  //------------------------------------------------------------------------------------------------
  public string BuildShader(EasyShader easyShader)
  {
    // Name it if necessary
    if(easyShader.shaderName == "")
      easyShader.shaderName = easyShader.gameObject.name.Replace(" ", "_").Trim();

    // Check that there is a Material
    if(easyShader.material == null)
    {
      if(EditorUtility.DisplayDialog("Missing material", "There is no material set on this EasyShader component, automatically create one ?", "Yes", "No"))
      {
        Material material = new Material(Shader.Find("Diffuse"));
        string path = Path.Combine("Assets", Path.Combine("EasyShader", Path.Combine("Generated", easyShader.shaderName + ".mat")));
        AssetDatabase.CreateAsset(material, path);
        easyShader.material = material;
      }
      else
        return null;
    }

    // Reinit
    inputs_ = "";
    properties_ = "";
    imports_ = "";
    renderType_ = "";
    lightingModel_ = "";
    fallback_ = "";
    culling_ = "";
    diffuse_ = "";
    specular_ = "";
    normal_ = "";
    emission_ = "";
    queue_ = "";
    nextPasses_ = "";

    // Create it
    Properties(easyShader);
    RenderType(easyShader);
    Queue(easyShader);
    Culling(easyShader);
    LightingModel(easyShader);
    Inputs(easyShader);
    SurfaceShader(easyShader);
    Fallback(easyShader);
    NextPasses(easyShader);

    // Generate it
    string template = TEMPLATE;
    template = template.Replace("$SHADER_NAME$", easyShader.shaderName);
    template = template.Replace("$PROPERTIES$", properties_);
    template = template.Replace("$RENDER_TYPE$", renderType_);
    template = template.Replace("$QUEUE$", queue_);
    template = template.Replace("$CULLING$", culling_);
    template = template.Replace("$IMPORTS$", imports_);
    template = template.Replace("$LIGHTING_MODEL$", lightingModel_);
    template = template.Replace("$INPUTS$", inputs_);
    template = template.Replace("$DIFFUSE$", diffuse_);
    template = template.Replace("$SPECULAR$", specular_);
    template = template.Replace("$NORMAL$", normal_);
    template = template.Replace("$EMISSION$", emission_);
    template = template.Replace("$FALLBACK$", fallback_);
    template = template.Replace("$NEXT_PASSES$", nextPasses_);
    
    return template;
  }

  //-------------------------------------------------------------------------------------
  // Properties
  //-------------------------------------------------------------------------------------
  private void Properties(EasyShader easyShader)
  {
    PropertiesBasicDiffuse(easyShader);
    PropertiesCutout(easyShader);
    PropertiesSpecularMap(easyShader);
    PropertiesNormalMap(easyShader);
    PropertiesRim(easyShader);
    PropertiesReflect(easyShader);
    PropertiesOutline(easyShader);
    PropertiesSelfIllum(easyShader);
  }

  private void PropertiesBasicDiffuse(EasyShader easyShader)
  {
    // Diffuse
    properties_ += @"
  _Color (""Main Color"", Color) = (1,1,1,1)";
    imports_ += "fixed4 _Color;\n";

    if(easyShader.transparent)
      properties_ += @"
  _MainTex (""Base (RGB) Alpha (A)"", 2D) = ""white"" {}";
    else
      properties_ += @"
  _MainTex (""Base (RGB)"", 2D) = ""white"" {}";
    imports_ += "sampler2D _MainTex;\n";

    // Specular
    if(easyShader.specular)
    {
      properties_ += @"
  _SpecColor (""Specular Color"", Color) = (0.5, 0.5, 0.5, 1)";

      properties_ += @"
  _Shininess (""Shininess"", Range (0.01, 1)) = 0.078125";
      imports_ += "half _Shininess;\n";

      properties_ += @"
  _Gloss(""Gloss"", Range (0.00, 1)) = .5";
      imports_ += "half _Gloss;\n";
    }
  }

  private void PropertiesCutout(EasyShader easyShader)
  {
    if(easyShader.transparent && easyShader.cutout)
      properties_ += @"
  _CutOut (""Cut Out"", Range(0, 1)) = 0.5";
  }

  private void PropertiesSpecularMap(EasyShader easyShader)
  {
    if(easyShader.specularMap)
    {
      if(easyShader.reflectMap)
        properties_ += @"
  _SpecMap (""Specular (R) Gloss (G) Reflect (B)"", 2D) = ""white"" {}";
      else
        properties_ += @"
  _SpecMap (""Specular (R) Gloss (G)"", 2D) = ""white"" {}";
      imports_ += "sampler2D _SpecMap;\n";
    }
  }

  private void PropertiesNormalMap(EasyShader easyShader)
  {
    if(easyShader.normalMap)
    {
      properties_ += @"
  _BumpMap (""Normal map"", 2D) = ""bump"" {}";
      imports_ += "sampler2D _BumpMap;\n";
    }
  }

  private void PropertiesSelfIllum(EasyShader easyShader)
  {
    if(easyShader.selfIllum)
    {
      properties_ += @"
  _Illum (""Illumin (A)"", 2D) = ""white"" {}";
      imports_ += "sampler2D _Illum;\n";
    }
  }

  private void PropertiesRim(EasyShader easyShader)
  {
    if(easyShader.rim)
    {
      properties_ += @"
  _RimColor (""Rim Color"", Color) = (0.26,0.19,0.16,0.0)
  _RimPower (""Rim Power"", Range(0.5,8.0)) = 3.0
      ";
      imports_ += @"float4 _RimColor;
                    half _RimPower;";
    }
  }

  private void PropertiesReflect(EasyShader easyShader)
  {
    if(easyShader.reflect)
    {
      properties_ += @"
  _Reflect(""Reflect"", Range (0.00, 1)) = .5
  _Cube (""Cubemap"", CUBE) = """" {}
      ";
      imports_ += @"
half _Reflect;
samplerCUBE _Cube;
      ";
      if(! easyShader.specularMap && easyShader.reflectMap)
      {
        properties_ += @"
  _ReflectMap(""Reflect map (R)"", 2D) = ""white"" {}";
        imports_ += "sampler2D _ReflectMap;";
      }
    }
  }

  private void PropertiesOutline(EasyShader easyShader)
  {
    if(easyShader.outline)
    {
      properties_ += @"
  _OutlineColor (""Outline Color"", Color) = (0,0,0,1)
  _Outline (""Outline width"", Range (.002, 0.03)) = .005";
    }
  }

  //-------------------------------------------------------------------------------------
  // RenderType
  //-------------------------------------------------------------------------------------
  private void RenderType(EasyShader easyShader)
  {
    if(easyShader.transparent)
      renderType_ += "Transparent";
    else
      renderType_ += "Opaque";
  }

  //-------------------------------------------------------------------------------------
  // Queue
  //-------------------------------------------------------------------------------------
  private void Queue(EasyShader easyShader)
  {
    if(easyShader.transparent)
      queue_ += "Transparent";
    else
      queue_ += "Geometry";
  }

  //-------------------------------------------------------------------------------------
  // Culling
  //-------------------------------------------------------------------------------------
  private void Culling(EasyShader easyShader)
  {
    if(easyShader.twoSided)
      culling_ += "Cull Off";
  }

  //-------------------------------------------------------------------------------------
  // LightingModel
  //-------------------------------------------------------------------------------------
  private void LightingModel(EasyShader easyShader)
  {
    string algo = easyShader.specular ? "BlinnPhong" : "Lambert";

    if(easyShader.transparent)
    {
      if(easyShader.cutout)
        lightingModel_ += " "+algo+"  alphatest:_CutOut ";
      else
        lightingModel_ += " "+algo+" alpha ";
    }
    else
      lightingModel_ += " "+algo+" ";
  }

  //-------------------------------------------------------------------------------------
  // Inputs
  //-------------------------------------------------------------------------------------
  private void Inputs(EasyShader easyShader)
  {
    if(easyShader.reflect)
      inputs_ += @"
  float3 worldRefl;";

    if(easyShader.rim)
      inputs_ += @"
  float3 viewDir;";

    if(easyShader.selfIllum)
      inputs_ +=  @"
  float2 uv_Illum;";
  }

  //-------------------------------------------------------------------------------------
  // SurfaceShader
  //-------------------------------------------------------------------------------------
  private void SurfaceShader(EasyShader easyShader)
  {
    ShaderDiffuse(easyShader);
    ShaderAlpha(easyShader);
    ShaderSpecular(easyShader);
    ShaderNormal(easyShader);
    ShaderEmission(easyShader);
  }

  private void ShaderDiffuse(EasyShader easyShader)
  {
    diffuse_ +=       @"
  fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
  o.Albedo = tex.rgb * _Color.rgb;
    ";
  }

  private void ShaderAlpha(EasyShader easyShader)
  {
    if(easyShader.transparent)
      diffuse_ += @"
  o.Alpha = tex.a * _Color.a;";
  }

  private void ShaderSpecular(EasyShader easyShader)
  {
    if(easyShader.specular && easyShader.specularMap)
    {
      specular_ += @"
  fixed4 spec = tex2D(_SpecMap, IN.uv_MainTex);
  o.Specular = spec.r * _Shininess;
  o.Gloss = spec.g * _Gloss;
      ";
    }
    else if(easyShader.specular)
    {
     specular_ += @"
  o.Specular = _Shininess;
  o.Gloss = _Gloss;";
    }
  }

  private void ShaderNormal(EasyShader easyShader)
  {
    if(easyShader.normalMap)
      normal_ += @"
  float3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
  o.Normal = normal;
      ";
  }

  private void ShaderEmission(EasyShader easyShader)
  {
    string selfIllum = "";
    if(easyShader.selfIllum)
      selfIllum = "tex2D(_Illum, IN.uv_Illum).a + ";

    if(easyShader.reflect && easyShader.rim)
    {
      if(easyShader.reflectMap)
      {
        string reflectMap;
        if(easyShader.specularMap)
          reflectMap = "spec.b";
        else
          reflectMap = "tex2D(_ReflectMap, IN.uv_MainTex).x;";
        string emission = @"
  half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
  o.Emission = "+selfIllum+@" texCUBE(_Cube, IN.worldRefl).rgb * _Reflect * $REFLECT_MAP$ + _RimColor.rgb * pow (rim, _RimPower);
        ";
        emission_ += emission.Replace("$REFLECT_MAP$", reflectMap);
      }
      else
      {
        emission_ += @"
  half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
  o.Emission = "+selfIllum+@" texCUBE(_Cube, IN.worldRefl).rgb * _Reflect + _RimColor.rgb * pow (rim, _RimPower);
        ";
      }
    }
    else if(easyShader.reflect)
    {
      emission_ += @"
  o.Emission = "+selfIllum+@" texCUBE(_Cube, IN.worldRefl).rgb * _Reflect;";
    }
    else if(easyShader.rim)
    {
      emission_ += @"
  half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
  o.Emission = "+selfIllum+@" _RimColor.rgb * pow (rim, _RimPower);
      ";
    }
    else if(easyShader.selfIllum)
    {
      emission_ += @"
  o.Emission = "+selfIllum+" 0;";
    }
  }

  //-------------------------------------------------------------------------------------
  // Fallback
  //-------------------------------------------------------------------------------------
  private void Fallback(EasyShader easyShader)
  {
    string light = easyShader.specular ? "Specular" : "Diffuse";

    if(easyShader.normalMap)
      light = "Bumped " + light;

    if(easyShader.reflect)
      fallback_ += "Self-Illumin/"+light;
    if(easyShader.selfIllum)
      fallback_ += "Reflective/"+light;
    else if(easyShader.transparent)
      fallback_ += "Transparent/"+light;
    else
      fallback_ += light;
  }

  //-------------------------------------------------------------------------------------
  // NextPasses
  //-------------------------------------------------------------------------------------
  private void NextPasses(EasyShader easyShader)
  {
    if(easyShader.outline)
      OutlinePass(easyShader);
  }

  private void OutlinePass(EasyShader easyShader)
  {
    nextPasses_ += @"
  Pass {
    Name ""OUTLINE""
    Tags { ""LightMode"" = ""Always"" ""Queue"" = ""Overlay"" }
    Cull Front
    ZWrite On
    ZTest LEqual
    ColorMask RGB
    Blend SrcAlpha OneMinusSrcAlpha
    Offset 15,15
 
    CGPROGRAM
    #include ""UnityCG.cginc""
 
    struct appdata {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
    };
     
    struct v2f {
        float4 pos : POSITION;
        float4 color : COLOR;
    };
     
    uniform float _Outline;
    uniform float4 _OutlineColor;
     
    v2f vert(appdata v) {
      v2f o;
      o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
     
      float3 norm   = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
      float2 offset = TransformViewToProjection(norm.xy);
     
      o.pos.xy += offset * o.pos.z * _Outline;
      o.color = _OutlineColor;
      return o;
    }

    #pragma vertex vert
    #pragma fragment frag
    half4 frag(v2f i) :COLOR { return i.color; }
   ENDCG
  }
    ";
  }

//-------------------------------------------------------------------------------------
// Template
//-------------------------------------------------------------------------------------

string TEMPLATE = @"
Shader ""EasyShader/Generated/$SHADER_NAME$"" {
Properties {
  $PROPERTIES$
}
SubShader { 
  Tags { ""RenderType""=""$RENDER_TYPE$"" ""Queue""=""$QUEUE$"" ""IgnoreProjector""=""True"" }
  LOD 400
  $CULLING$
  
CGPROGRAM
#pragma surface surf $LIGHTING_MODEL$
#pragma target 3.0

$IMPORTS$

struct Input {
  float2 uv_MainTex;
  $INPUTS$
  INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o) {
  $DIFFUSE$
  $SPECULAR$
  $NORMAL$
  $EMISSION$
}
ENDCG
$NEXT_PASSES$
}
FallBack ""$FALLBACK$""
}
";
}