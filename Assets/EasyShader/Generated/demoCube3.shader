
Shader "EasyShader/Generated/demoCube3" {
Properties {
  
  _Color ("Main Color", Color) = (1,1,1,1)
  _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
  _Reflect("Reflect", Range (0.00, 1)) = .5
  _Cube ("Cubemap", CUBE) = "" {}
      
}
SubShader { 
  Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="True" }
  LOD 400
  Cull Off
  
CGPROGRAM
#pragma surface surf  Lambert alpha 
#pragma target 3.0

fixed4 _Color;
sampler2D _MainTex;

half _Reflect;
samplerCUBE _Cube;
      

struct Input {
  float2 uv_MainTex;
  
  float3 worldRefl;
  INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o) {
  
  fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
  o.Albedo = tex.rgb * _Color.rgb;
    
  o.Alpha = tex.a * _Color.a;
  
  
  
  o.Emission =  texCUBE(_Cube, IN.worldRefl).rgb * _Reflect;
}
ENDCG

}
FallBack "Self-Illumin/DiffuseTransparent/Diffuse"
}
