
Shader "EasyShader/Generated/Pyramids_shade" {
Properties {
  
  _Color ("Main Color", Color) = (1,1,1,1)
  _MainTex ("Base (RGB)", 2D) = "white" {}
  _SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
  _Shininess ("Shininess", Range (0.01, 1)) = 0.078125
  _Gloss("Gloss", Range (0.00, 1)) = .5
  _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
  _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
      
}
SubShader { 
  Tags { "RenderType"="Opaque" "Queue"="Geometry" "IgnoreProjector"="True" }
  LOD 400
  
  
CGPROGRAM
#pragma surface surf  BlinnPhong 
#pragma target 3.0

fixed4 _Color;
sampler2D _MainTex;
half _Shininess;
half _Gloss;
float4 _RimColor;
                    half _RimPower;

struct Input {
  float2 uv_MainTex;
  
  float3 viewDir;
  INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o) {
  
  fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
  o.Albedo = tex.rgb * _Color.rgb;
    
  
  o.Specular = _Shininess;
  o.Gloss = _Gloss;
  
  
  half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
  o.Emission =  _RimColor.rgb * pow (rim, _RimPower);
      
}
ENDCG

}
FallBack "Specular"
}
