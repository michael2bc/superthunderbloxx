// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "EasyShader/Generated/demoCube1" {
Properties {
  
  _Color ("Main Color", Color) = (1,1,1,1)
  _MainTex ("Base (RGB)", 2D) = "white" {}
  _BumpMap ("Normal map", 2D) = "bump" {}
  _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
  _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
      
  _OutlineColor ("Outline Color", Color) = (0,0,0,1)
  _Outline ("Outline width", Range (.002, 0.03)) = .005
}
SubShader { 
  Tags { "RenderType"="Opaque" "Queue"="Geometry" "IgnoreProjector"="True" }
  LOD 400
  
  
CGPROGRAM
#pragma surface surf  Lambert 
#pragma target 3.0

fixed4 _Color;
sampler2D _MainTex;
sampler2D _BumpMap;
float4 _RimColor;
                    half _RimPower;

struct Input {
  float2 uv_MainTex;
  
  float3 viewDir;
  INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o) {
  
  fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
  o.Albedo = tex.rgb * _Color.rgb;
    
  
  
  float3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
  o.Normal = normal;
      
  
  half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
  o.Emission =  _RimColor.rgb * pow (rim, _RimPower);
      
}
ENDCG

  Pass {
    Name "OUTLINE"
    Tags { "LightMode" = "Always" "Queue" = "Overlay" }
    Cull Front
    ZWrite On
    ZTest LEqual
    ColorMask RGB
    Blend SrcAlpha OneMinusSrcAlpha
    Offset 15,15
 
    CGPROGRAM
    #include "UnityCG.cginc"
 
    struct appdata {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
    };
     
    struct v2f {
        float4 pos : POSITION;
        float4 color : COLOR;
    };
     
    uniform float _Outline;
    uniform float4 _OutlineColor;
     
    v2f vert(appdata v) {
      v2f o;
      o.pos = UnityObjectToClipPos(v.vertex);
     
      float3 norm   = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
      float2 offset = TransformViewToProjection(norm.xy);
     
      o.pos.xy += offset * o.pos.z * _Outline;
      o.color = _OutlineColor;
      return o;
    }

    #pragma vertex vert
    #pragma fragment frag
    half4 frag(v2f i) :COLOR { return i.color; }
   ENDCG
  }
    
}
FallBack "Bumped Diffuse"
}
