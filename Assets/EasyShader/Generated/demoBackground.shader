
Shader "EasyShader/Generated/demoBackground" {
Properties {
  
  _Color ("Main Color", Color) = (1,1,1,1)
  _MainTex ("Base (RGB)", 2D) = "white" {}
}
SubShader { 
  Tags { "RenderType"="Opaque" "Queue"="Geometry" "IgnoreProjector"="True" }
  LOD 400
  
  
CGPROGRAM
#pragma surface surf  Lambert 
#pragma target 3.0

fixed4 _Color;
sampler2D _MainTex;


struct Input {
  float2 uv_MainTex;
  
};

void surf (Input IN, inout SurfaceOutput o) {
  
  fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
  o.Albedo = tex.rgb * _Color.rgb;
    
  
  
  
}
ENDCG

}
FallBack "Diffuse"
}
