
Shader "EasyShader/Generated/Post_shade" {
Properties {
  
  _Color ("Main Color", Color) = (1,1,1,1)
  _MainTex ("Base (RGB) Alpha (A)", 2D) = "white" {}
  _SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
  _Shininess ("Shininess", Range (0.01, 1)) = 0.078125
  _Gloss("Gloss", Range (0.00, 1)) = .5
  _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
  _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
      
  _Reflect("Reflect", Range (0.00, 1)) = .5
  _Cube ("Cubemap", CUBE) = "" {}
      
}
SubShader { 
  Tags { "RenderType"="Transparent" "Queue"="Transparent" "IgnoreProjector"="True" }
  LOD 400
  
  
CGPROGRAM
#pragma surface surf  BlinnPhong alpha 
#pragma target 3.0

fixed4 _Color;
sampler2D _MainTex;
half _Shininess;
half _Gloss;
float4 _RimColor;
                    half _RimPower;
half _Reflect;
samplerCUBE _Cube;
      

struct Input {
  float2 uv_MainTex;
  
  float3 worldRefl;
  float3 viewDir;
  INTERNAL_DATA
};

void surf (Input IN, inout SurfaceOutput o) {
  
  fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
  o.Albedo = tex.rgb * _Color.rgb;
    
  o.Alpha = tex.a * _Color.a;
  
  o.Specular = _Shininess;
  o.Gloss = _Gloss;
  
  
  half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
  o.Emission =  texCUBE(_Cube, IN.worldRefl).rgb * _Reflect + _RimColor.rgb * pow (rim, _RimPower);
        
}
ENDCG

}
FallBack "Self-Illumin/SpecularTransparent/Specular"
}
